﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;


namespace DiplomaProject
{
    public partial class Evaluate : System.Web.UI.Page
    {
        public int picOneVal = 1;
        public int picTwoVal;

        List<int> listID = new List<int>();

        //public void FillTheList()
        //{
        //    listID.Add(15);
        //    listID.Add(16);
        //    listID.Add(17);
        //    listID.Add(18);
        //    listID.Add(19);

        //}

        public int cnt_im = 0;

        public void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                Session["num_load"] = 1;
                using (SqlConnection connection = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True"))
                {
                    connection.Open();

                    if (Session["Trial"].ToString() != "Cats" &&
                        Session["Trial"].ToString() != "Nature" &&
                        Session["Trial"].ToString() != "MiNI")
                    {
                        DropDown1.Items.Clear();
                        DropDown2.Items.Clear();
                        string query = "SELECT DISTINCT algo FROM imagess where trial=@trial";
                        using (SqlCommand command = new SqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@trial", Session["Trial"].ToString());
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    DropDown1.Items.Add(reader.GetString(0));
                                    DropDown2.Items.Add(reader.GetString(0));
                                }
                            }
                        }
                    }

                    string query1 = "select max(id) from imagess where trial=@trial";
                    using (SqlCommand command = new SqlCommand(query1, connection))
                    {
                        command.Parameters.AddWithValue("@trial", Session["Trial"].ToString());
                        cnt_im = (int)command.ExecuteScalar();
                        Session["tot_count"] = cnt_im;
                    }

                    connection.Close();
                }
            }
            

            if (this.IsPostBack)
            {
                Label1.Text = Request.Form[hf1.UniqueID];
                Label2.Text = Request.Form[hf2.UniqueID];

                //Label1.Text = "1";
                //Label2.Text = "99";
            }
        }

        //OBTAIN NEXT PICTURE
        public string algorithm;
        public byte[] imageData;
        public byte[] imageData1;
        public byte[] imageData2;
        //public int number = 1;
        void getNext()
        {
            //string al1_n = DropDown1.Text;
            //string al2_n = DropDown2.Text;
            //Session["al1_n"] = al1_n;
            //Session["al2_n"] = al2_n;

            string trial = Session["Trial"].ToString();
            int total = Convert.ToInt32(Session["tot_count"]);


            //if (trial == "Cats")
            //    total = 44;

            //if (trial == "Nature")
            //    total = 19;

            //if (trial == "MiNI")
            //    total = 41;

            int number = Convert.ToInt32(Session["num_load"]);

            lbl.Text = number.ToString() + "/" + total.ToString();

            if (number == total+1)
                //((trial == "Cats" && number == 45) || (trial == "MiNI" && number == 42) || (trial == "Nature" && number == 20))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('END OF THE DATASET! REDIRECTING YOU TO THE MAIN PAGE')", true);
                //Session["isLoggedIn"] = null;
                //Session["Email"] = null;
                //Session["Trial"] = null;
                Session["num_load"] = null;
                Response.Redirect("Account.aspx");
            }

            //SqlConnection con = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_images;Integrated Security=True");
            SqlConnection con = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True");
            con.Open();

            using (SqlCommand cmd = new SqlCommand("select image from imagess where id=@id and algo=@algo and trial=@trial", con))
            {
                cmd.Parameters.AddWithValue("@id", number);
                cmd.Parameters.AddWithValue("@algo", DropDown1.Text);
                cmd.Parameters.AddWithValue("@trial", trial);
                imageData1 = (byte[])cmd.ExecuteScalar();
            }

            using (SqlCommand cmd = new SqlCommand("select image from imagess where id=@id and algo=@algo and trial=@trial", con))
            {
                cmd.Parameters.AddWithValue("@id", number);
                cmd.Parameters.AddWithValue("@algo", DropDown2.Text);
                cmd.Parameters.AddWithValue("@trial", trial);
                imageData2 = (byte[])cmd.ExecuteScalar();
            }

            Image1.ImageUrl = "data:image;base64," + Convert.ToBase64String(imageData1);
            Image2.ImageUrl = "data:image;base64," + Convert.ToBase64String(imageData2);


            con.Close();

            Session["num_load"] = Convert.ToInt32(Session["num_load"]) + 1;


            Label1.Text = "1";
            Label2.Text = "99";
        }

        //GET STARTED BUTTON
        public void Button3_Click1(object sender, EventArgs e)
        {
            //Button3.Enabled = false;
            Button3.Visible = false;
            Button1.Visible = true;
            getNext();
            //Session["num_load"] = Convert.ToInt32(Session["num_load"]) + 1;
        }

        //SUBMIT FUNCTION
        void doneSave()
        {
            int i1 = Int32.Parse(Label1.Text);
            int i2 = Int32.Parse(Label2.Text);


            //byte[] imageData = File.ReadAllBytes(Server.MapPath(Image1.ImageUrl.Remove(0, 18)));

            //SqlConnection con0 = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_images;Integrated Security=True");
            SqlConnection con0 = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True");
            con0.Open();


            using (SqlCommand cmd = new SqlCommand("INSERT INTO done_im (num, trial, al1, al1_name, al2, al2_name, usem, compby) VALUES (@num, @trial, @al1, @al1_name, @al2, @al2_name, @usem, @compby)", con0))
            {
                cmd.Parameters.AddWithValue("@num", (Convert.ToInt32(Session["num_load"]) - 1));
                cmd.Parameters.AddWithValue("@trial", Session["Trial"].ToString());
                //cmd.Parameters.AddWithValue("@trial", "trial");
                cmd.Parameters.AddWithValue("@al1", i1);
                cmd.Parameters.AddWithValue("@al2", i2);
                cmd.Parameters.AddWithValue("@al1_name", DropDown1.Text);
                cmd.Parameters.AddWithValue("@al2_name", DropDown2.Text);
                cmd.Parameters.AddWithValue("@usem", Session["Email"]);
                cmd.Parameters.AddWithValue("@compby", text1.Text);
                //cmd.Parameters.AddWithValue("@usem", "email");
                cmd.ExecuteNonQuery();

            }

            con0.Close();

            Label1.Text = "1";
            Label2.Text = "99";
        }
        //int imId;

        //SUBMIT BUTTON
        public void Button1_Click1(object sender, EventArgs e)
        {

            doneSave();
            getNext();
        }

        //SAVE TO DATABASE - MOVED TO "UPLOAD" IN THE ADMIN ACCOUNTS
        protected void Button2_Click1(object sender, EventArgs e)
        {
            //SqlConnection con = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_images;Integrated Security=True");

            //SqlConnection con = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True");
            //con.Open();
            //for (int i = 1; i <= 41; i++)
            //{
            //    using (SqlCommand cmd = new SqlCommand("INSERT INTO imagess (image, id, trial, algo) VALUES (@im2, @id, @trial, @algo)", con))
            //    {

            //        byte[] imageData = File.ReadAllBytes("C:/Users/julia/Desktop/pear_v2.0/DiplomaProject/algorithms_mini/unsharp/" + i.ToString() + ".jpg");
            //        //cmd.Parameters.AddWithValue("@im1", imageData);
            //        cmd.Parameters.AddWithValue("@im2", imageData);
            //        cmd.Parameters.AddWithValue("@id", i);
            //        cmd.Parameters.AddWithValue("@trial", "mini");
            //        cmd.Parameters.AddWithValue("@algo", "unsharp");
            //        cmd.ExecuteNonQuery();

            //    }
            //}

            //con.Close();

            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('INSERTED')", true);


        }

        protected void DropDown1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string al1_n = DropDown1.Text;
            //Session["al1_n"] = al1_n;
        }

        protected void DropDown2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string al2_n = DropDown2.Text;
            //Session["al2_n"] = al2_n;
        }

        //protected void Button5_Click(object sender, EventArgs e)
        //{
        //    Response.Write("ALL CHANGES DISCARDED");
        //    Session["isLoggedIn"] = null;
        //    Session["Email"] = null;
        //    Session["Trial"] = null;
        //    Response.Redirect("MainPage.aspx");
        //}
    }
}