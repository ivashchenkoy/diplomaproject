﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiplomaProject
{
    public partial class SavedForLater : System.Web.UI.Page
    {
        ICollection CreateDataSource()
        {
            //SqlConnection con = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_images;Integrated Security=True");
            SqlConnection con = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True");
            //con.Open();

            SqlCommand cmd;
            //SqlDataAdapter adap = new SqlDataAdapter();
            String sql = "";
            sql = "select num as ___No___, trial as ___Trial___, al1_name as ___Algorithm_1___, al1 as ___Score_1___,al2_name as ___Algorithm_2___, al2 as ___Score_2___, compby as ___Compared_by___ from done_im where usem=@mymail";

            cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("mymail", Session["Email"]);
            //cmd.Parameters.AddWithValue("mymail", "email");

            //cmd.ExecuteNonQuery();

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            con.Open();
            /*int i = */
            cmd.ExecuteNonQuery();
            con.Close();

            //DataTable dt = new DataTable();
            //DataRow dr;

            //dt.Columns.Add(new DataColumn("___No. of image___", typeof(Int32)));
            //dt.Columns.Add(new DataColumn("______Trial_____", typeof(string)));
            //dt.Columns.Add(new DataColumn("___Raw Score___", typeof(string)));
            //dt.Columns.Add(new DataColumn("__Modified Score__", typeof(string)));

            //ButtonField bf = new ButtonField();
            //bf.Text = "button";
            //bf.ButtonType = ButtonType.Button;
            //bf.HeaderText = "button";
           

            //for (int i = 0; i < 9; i++)
            //{
            //    dr = dt.NewRow();

            //    dr[0] = i;
            //    dr[1] = "trial";
            //    dr[2] = "1%";
            //    dr[3] = "99%";

            //    dt.Rows.Add(dr);
            //}

            DataView dv = new DataView(dt);

            return dv;
        }

        public void Page_Load(object sender, EventArgs e)
        {
            //ButtonColumn bc = new ButtonColumn();

            //ButtonField bc = new ButtonField();

            //bc.Text = "press";
            //bc.HeaderText = "__Press to continue__";
            //bc.ButtonType = ButtonType.Button;
            //dg2.Columns.Add(bc);

            dg2.DataSource = CreateDataSource();

            dg2.DataBind();

            //string email;
            //email = Session["Email"].ToString();

            label1.Text = "Results for user " + Session["Email"];
        }
    }
}