﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="DiplomaProject.SignUp" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>SIGN UP</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/images/pear_icon.ico" />
    <link rel="stylesheet" type="text/css" href="util.css">
    <link rel="stylesheet" type="text/css" href="Style_SignUp.css">
</head>
<body runat="server">

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form runat="server" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    <span class="login100-form-title">
                        SIGN UP
                    </span>

                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter your name">
                        <asp:TextBox ID="TextBox1" Text="" runat="server" class="input100" type="text" name="name" placeholder="Name"/>
                        <span class="focus-input100"></span>
                    </div>


                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter your email">
                        <asp:TextBox ID="TextBox2" Text="" runat="server" class="input100" type="text" name="email" placeholder="Email"/>
                        <span class="focus-input100"></span>
                    </div>

<%--                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter your phone number with country code">

                        <asp:TextBox ID="TextBox3" runat="server" class="input100" type="number" name="number" placeholder="Phone number"/>
                        <span class="focus-input100"></span>
                    </div>--%>

                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter password">
                        <asp:TextBox ID="TextBox4" Text="" runat="server" class="input100" type="password" name="pass" placeholder="Password"/>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter password again">
                        <%--<input class="input100" type="password" name="reppass" placeholder="Repeat password">
                        --%>
                        <asp:TextBox ID="TextBox5" Text="" runat="server" class="input100" type="password" name="reppass" placeholder="Repeat password"/>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="m-t-10 m-b-10">
                        <asp:Label ID="Label1" Text="" runat="server" />
                    </div>

                    <div runat="server" class="container-login100-form-btn">
                        <asp:Button ID="Button1" runat="server" class="login100-form-btn" Text="SIGN UP" OnClick="Button1_Click1" />
                        <%--<button class="login100-form-btn" onclick="">
                            Sign up
                        </button>--%>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script type ="text/javascript">
    </script>

</body>
</html>
