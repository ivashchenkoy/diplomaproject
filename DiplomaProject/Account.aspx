﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account.aspx.cs" Inherits="DiplomaProject.Account" %>

<!DOCTYPE html>

<html>
<head>
    <title>YOUR ACCOUNT</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="shortcut icon" type="image/png" href="images/pear_icon.ico" />
    <link rel="stylesheet" href="Style_MainPage.css" />
    <link rel="stylesheet" type="text/css" href="util.css">
    <link rel="stylesheet" type="text/css" href="Style_SignUp.css">

</head>
<body>
    <header id="header">
        <div class="inner">
            <a href="MainPage.aspx" class="logo">PEAR: PicturE Analysis Research</a>
            <nav id="nav">
                <a href="LearnMore.html">learn more</a>
                <!--<a href="MainPage.aspx">main page</a>-->
            </nav>
        </div>
    </header>

    <div class="limiter">
    <div class="container-login100">
            <div class="wrap-login100">
                <center>
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                </center>
               
        

            

        <div class="m-t-15">
            <a href="Evaluate.aspx">
                <button class="login100-form-btn">
                    GET STARTED</button>
            </a>
        </div>

        <div class="m-t-15">
            <a href="SavedForLater.aspx">
                <button class="login100-form-btn">
                    RESULTS
                </button>
            </a>
        </div>

        <div class="m-t-15">
            <a href="GetHelp.html">
                <button class="login100-form-btn">
                    GET HELP
                </button>
            </a>
        </div>

<form runat="server">
        <div class="m-t-15">
<%--            <a href="Upload.aspx">
                <button id="but1" class="login100-form-btn">
                    UPLOAD YOUR OWN DATASET
                </button>
            </a>--%>
            <asp:LinkButton ID="but1" Visible="true" CssClass="login100-form-btn" runat="server" Text="UPLOAD DATASET" OnClick="but1_Click">
                </asp:LinkButton>
        </div>

        
             <asp:Label runat="server" ID="lab" Text="CHANGE TRIAL HERE:"></asp:Label>
            <asp:dropdownlist runat="server" id="DropDown1" AutoPostBack="true" OnSelectedIndexChanged="DropDown1_SelectedIndexChanged"> 
            </asp:dropdownlist>

        <div class="m-t-15">
            
                <asp:LinkButton ID="b1" CssClass="login100-form-btn" runat="server" Text="LOG OUT" OnClick="b1_Click">
                </asp:LinkButton>
            
        </div>
        </form>
            <center>
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            </center>
    </div>
</div></div>

</body>
</html>
