﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace DiplomaProject
{
    public partial class Account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //DropDown1.ClearSelection();
            //DropDown1.Items.Insert(0, new ListItem(string.Empty, string.Empty));
            //DropDown1.SelectedIndex = 0;

            //DropDown1.SelectedValue = Session["Trial"].ToString();

            but1.Visible = false;
            if (((String)Session["Role"]).Contains("admin") == true)
            {
                but1.Visible = true;
            }

            string rol = (String)Session["Role"];
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(rol)", true);

            if (!this.IsPostBack)
            {
                //DropDown1.SelectedIndex = DropDown1.Items.IndexOf(DropDown1.Items.FindByValue(Session["Trial"].ToString()));

                using (SqlConnection connection = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True"))
                {
                    connection.Open();
                    string query = "SELECT DISTINCT trial FROM imagess";
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DropDown1.Items.Add(reader.GetString(0));
                            }

                        }
                    }

                    //DropDown1.SelectedIndex = DropDown1.Items.IndexOf(DropDown1.Items.FindByValue(Session["Trial"].ToString()));
                    connection.Close();
                }
                DropDown1.SelectedIndex = DropDown1.Items.IndexOf(DropDown1.Items.FindByValue(Session["Trial"].ToString()));
            }

        }

        protected void b1_Click(object sender, EventArgs e)
        {
            //Response.Write("ALL CHANGES DISCARDED");
            Session["isLoggedIn"] = null;
            Session["Email"] = null;
            Session["Trial"] = null;
            Response.Redirect("MainPage.aspx");
        }

        protected void DropDown1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Trial"] = DropDown1.SelectedValue.ToString();
        }

        protected void but1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Upload.aspx");
        }
    }
}