﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiplomaProject
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            //SqlConnection con1 = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_users;Integrated Security=True");
            SqlConnection con1 = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_users;Integrated Security=True");
            con1.Open();

            SqlCommand cmd1 = new SqlCommand("select count(*) from userss where email=@myemail", con1);            
            cmd1.Parameters.AddWithValue("@myemail", TextBox2.Text);
            int cnt = (int)cmd1.ExecuteScalar();
            
            con1.Close();

            if (cnt > 0)
            {
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = "THIS EMAIL IS ALREADY REGISTERED";
            }
            else
            {

                if (TextBox1.Text == "" || TextBox2.Text == "" ||
                    TextBox4.Text == "" || TextBox5.Text == "")
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "SOME REQUIRED FIELDS ARE EMPTY";
                }

                if (TextBox4.Text != TextBox5.Text)
                {
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = "PASSWORDS DON'T MATCH";
                    TextBox4.Text = "";
                    TextBox5.Text = "";
                }

                if (TextBox1.Text != "" && TextBox2.Text != "" &&
                    TextBox4.Text != "" && TextBox5.Text != "" && TextBox4.Text == TextBox5.Text && cnt == 0)
                {
                    //SqlConnection con = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_users;Integrated Security=True");
                    SqlConnection con = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_users;Integrated Security=True");
                    con.Open();

                    SqlCommand cmd;
                    SqlDataAdapter adap = new SqlDataAdapter();
                    String sql = "";
                    sql = "INSERT INTO userss(name, email, pass, role) VALUES (@myname, @mymail, @mypass, 'user')";


                    cmd = new SqlCommand(sql, con);
                    //cmd.Parameters.AddWithValue("@myname", Request.QueryString[TextBox4.Text]);

                    cmd.Parameters.AddWithValue("myname", TextBox1.Text);
                    cmd.Parameters.AddWithValue("mymail", TextBox2.Text);
                    cmd.Parameters.AddWithValue("mypass", TextBox4.Text);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('REGISTERED SUCCESSFULLY')", true);
                    Response.Redirect("LogIn.aspx");

                    //adap.InsertCommand = new SqlCommand(sql, con);
                    //adap.InsertCommand.ExecuteNonQuery();


                }
            }
        }
    }
}