﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="DiplomaProject.MainPage" %>

<!DOCTYPE html>
<html>

<head runat="server">
    <title>PEAR</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="shortcut icon" type="image/png" href="/images/pear_icon.ico" />
    <link rel="stylesheet" href="Style_MainPage.css" />
    <link rel="stylesheet" href="util.css" />

</head>

<body>

    <header id="header">
        <div class="inner">
            <a href="#" class="logo">PEAR: PicturE Analysis Research</a>
                <nav id="nav">

                    <%--<a  href="LearnMore.html">learn more</a>
                    <a id="lin" href="LogIn.aspx">log in!</a>--%>
                    <a>
                        <asp:HyperLink id="HyperLink3" Visible="false" NavigateUrl="Account.aspx" Text="your account" class="txt3" runat="server"></asp:HyperLink>
                    </a>

                    <a>
                        <asp:HyperLink id="HyperLink2" NavigateUrl="LearnMore.html" Text="learn more" class="txt3" runat="server"></asp:HyperLink>
                    </a>

                    <a>
                        <asp:HyperLink id="HyperLink1" visible="true" NavigateUrl="LogIn.aspx" Text="Log in!" class="txt3" runat="server"></asp:HyperLink>
                    </a>
                </nav>
            
        </div>
    </header>
    
    <div class="m-t-100">
    <center>
    <img src="images/giphy.gif" height="300" width="300">
    <img src="images/giphy.gif" height="300" width="300">
    <img src="images/giphy.gif" height="300" width="300">
    </center>
    </div>
    <center>
    HELLO AND WELCOME!!
    <br><asp:HyperLink NavigateUrl="GetHelp.html" runat="server">CLICK FOR INSTRUCTION</asp:HyperLink>
    </center>
    <form runat="server">
    <asp:Label ID="lab1" runat="server"></asp:Label>
        </form>
<%--    <section id="banner">
        <div class="inner">
            <form runat="server">
                <div class="m-t-10 m-b-300">
                <h1> </h1>
                </div>

            <ul class="actions">
                   <asp:Button runat="server" ID="gs1" Text="GET STARTED" class="button alt" OnClick="gs1_Click1" />
                    <!--<a href="Account.html" class="button alt">get started</a>-->
                
            </ul>
            </form>
        </div>
    </section>--%>

 <%--   <section id="footer">
        
        <div class="inner">
            <header>
                <h2>GET IN TOUCH!</h2>
            </header>
            <form method="post" action="#">
                <div class="field half first">
                    <label for="name">name</label>
                    <input type="text" name="name" id="name" />
                </div>
                <div class="field half">
                    <label for="email">subject</label>
                    <input type="text" name="subj" id="subj" />
                </div>
                <div class="field">
                    <label for="message">message</label>
                    <textarea name="message" id="message" rows="6"></textarea>
                </div>
                <ul class="actions">
                    <li><input type="button" onclick="sendMail(); return false" value="send message" class="alt" id="ShowButton" /></li>
                </ul>
            </form>
        </div>
    </section>--%>

    <!--<script src="https://smtpjs.com/v3/smtp.js">
    </script>
    <script>
        Email.send({
            Host: "194.29.178.21:21345",
            Username: "julia",
            Password: "JUli4@0!",
            To: "ivashchenkoy@student.mini.pw.edu.pl",
            From: "ivashchenkoy@student.mini.pw.edu.pl",
            Subject: "This is the subject",
            Body: "And this is the body"
        }).then(
            message => alert(message)
        );
    </script>-->

    <!--<a href="mailto:ivashchenkoy@student.mini.pw.edu.pl">email me here!</a>-->

    <script>
        function sendMail() {
            var link = "mailto:ivashchenkoy@student.mini.pw.edu.pl"
                + "?cc=karpovad@student.mini.pw.edu.pl"
                + "&subject=" + escape(document.getElementById('subj').value)
                + "&body=" + escape(document.getElementById('message').value)
                ;

            window.location.href = link;
            //document.getElementById('ShowButton').value = 'SENT';
        }

    </script>
</body>
</html>
