﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="DiplomaProject.LogIn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>LOG IN</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/images/pear_icon.ico" />
    <link rel="stylesheet" type="text/css" href="util.css">
    <link rel="stylesheet" type="text/css" href="Style_SignUp.css">
</head>
<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form runat="server" class="login100-form validate-form p-l-55 p-r-55 p-t-178">
                    <span class="login100-form-title">
                        LOG IN
                    </span>

                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter your email">
                        <asp:TextBox ID="TextBox1" runat="server" class="input100" type="text" name="email" placeholder="Email"/>
                        <span class="focus-input100"></span>
                    </div>

                    <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter password">
                        <asp:TextBox ID="TextBox2" runat="server" class="input100" type="password" name="pass" placeholder="Password"/>
                        <span class="focus-input100"></span>
                    </div>
                    
                    <div class="m-t-10 m-b-10">
                        <asp:Label ID="Label2" Text="Choose a trial:" runat="server" />
                    </div>

                    <%--<asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" />--%>

                    <asp:dropdownlist runat="server" id="DropDown1"> 
                         <%--<asp:listitem text="Cats" value="cats"></asp:listitem>--%>
                    </asp:dropdownlist>

                    <div class="m-t-10 m-b-10">
                        <asp:Label ID="Label1" Text="" runat="server" />
                    </div>

                    <div runat="server" class="container-login100-form-btn">
                        <asp:Button ID="Button1" runat="server" class="login100-form-btn" Text="LOG IN" OnClick="Button1_Click"/>
                    </div>


                    <div class="flex-col-c p-t-20 p-b-10">
                        <span class="txt1 p-b-9">
                            Don’t have an account?
                        </span>
                        <asp:HyperLink id="HyperLink1" NavigateUrl="SignUp.aspx" Text="Sign up now" class="txt3" runat="server"/> 
                        <%--<a href="SignUp.html" class="txt3">
                            Sign up now
                        </a>--%>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>