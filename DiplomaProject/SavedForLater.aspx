﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SavedForLater.aspx.cs" Inherits="DiplomaProject.SavedForLater" %>

<!DOCTYPE html>

<html>
<head>
    <title>SAVED PICTURES</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="shortcut icon" type="image/png" href="images/pear_icon.ico" />
    <link rel="stylesheet" href="Style_MainPage.css" />
    <link rel="stylesheet" type="text/css" href="util.css">

</head>
<body>
    <header id="header">
        <div class="inner">
            <a href="MainPage.aspx" class="logo">PEAR: PicturE Analysis Research</a>
            <nav id="nav">
                <a href="LearnMore.html">learn more</a>
                <a href="Account.aspx">your account</a>
            </nav>
        </div>
    </header>

    <div style="margin-left:auto; margin-right:auto;">
        <form class="m-l-130 m-t-50" runat="server">
            <img src="images/pear_logo.png" height="50" width="50">
            <asp:Label id="label1" Text="" runat="server"></asp:Label>

            
            <asp:GridView ID="dg2" BorderColor="black"
           BorderWidth="5" BorderStyle="Double"
           CellPadding="3"
           AutoGenerateColumns="true" runat="server">
                <%--<Columns>
                 <asp:ButtonColumn  Text="press" HeaderText="___Press to continue___" CommandName="details" ButtonType="PushButton"></asp:ButtonColumn>
                </Columns>--%>
            </asp:GridView>

        </form>
        </div>

</body>
</html>
