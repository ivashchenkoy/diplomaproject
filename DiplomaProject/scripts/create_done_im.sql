USE [pear_images]
GO

/****** Object:  Table [dbo].[done_im]    Script Date: 4/28/2020 4:27:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[done_im](
	[num] [int] NOT NULL,
	[trial] [nvarchar](50) NOT NULL,
	[al1] [int] NOT NULL,
	[al1_name] [nvarchar](50) NULL,
	[al2] [int] NOT NULL,
	[al2_name] [nvarchar](50) NULL,
	[usem] [nvarchar](50) NOT NULL,
	[compby] [nvarchar](50) NULL
) ON [PRIMARY]
GO

