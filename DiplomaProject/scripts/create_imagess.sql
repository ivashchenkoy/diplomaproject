USE [pear_images]
GO

/****** Object:  Table [dbo].[imagess]    Script Date: 4/28/2020 4:27:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[imagess](
	[image] [varbinary](max) NOT NULL,
	[id] [int] NOT NULL,
	[trial] [nvarchar](50) NOT NULL,
	[algo] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

