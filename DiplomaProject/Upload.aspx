﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="DiplomaProject.Upload" %>

<!DOCTYPE html>

<html>
<head>
    <title>UPLOAD YOUR DATASET</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="shortcut icon" type="image/png" href="images/pear_icon.ico" />
    <link rel="stylesheet" href="Style_MainPage.css" />
    <link rel="stylesheet" type="text/css" href="util.css">
    <link rel="stylesheet" type="text/css" href="Style_SignUp.css">

</head>
<body>
    <header id="header">
        <div class="inner">
            <a href="MainPage.aspx" class="logo">PEAR: PicturE Analysis Research</a>
            <nav id="nav">
                <a href="Account.aspx">YOUR ACCOUNT</a>
                <!--<a href="MainPage.aspx">main page</a>-->
            </nav>
        </div>
    </header>

    <div class="limiter">
    <div class="container-login100">
            <div class="wrap-login100">
                <center>
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
                </center>
               
        <form runat="server" class="login100-form validate-form">
             <asp:Label runat="server" Text="TRIAL:"></asp:Label>

             <div class="wrap-input100 validate-input">
            <asp:TextBox runat="server" class="input100" type="text" ID="TextBox1"></asp:TextBox>
<%--                  <span class="focus-input100"></span>--%>
            </div>

            <asp:Label runat="server" Text="ALGORITHM:"></asp:Label>

             <div class="wrap-input100 validate-input">
            <asp:TextBox runat="server" class="input100" type="text" ID="TextBox2"></asp:TextBox>
<%--                  <span class="focus-input100"></span>--%>
            </div>
<%--        <div class="m-t-15">          
                <asp:LinkButton ID="b1" CssClass="login100-form-btn" runat="server" Text="SELECT PICTURES" OnClick="b1_Click">
                </asp:LinkButton>           
        </div>--%>

        <asp:FileUpload id="UploadImages"  AllowMultiple="true" runat="server" />
            <div class="m-t-15">          
                <asp:LinkButton ID="LinkButton10" CssClass="login100-form-btn" runat="server" Text="UPLOAD TO DATABASE" OnClick="LinkButton10_Click" >
                </asp:LinkButton>           
        </div>

        </form>

            <center>
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            <img class="m-t-10" src="images/pear_logo.png" height="50" width="50">
            </center>
    </div>
</div></div>

</body>
</html>