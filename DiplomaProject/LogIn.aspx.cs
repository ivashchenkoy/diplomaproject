﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace DiplomaProject
{


    public partial class LogIn : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            using (SqlConnection connection = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True"))
            {
                connection.Open();
                string query = "SELECT DISTINCT trial FROM imagess";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DropDown1.Items.Add(reader.GetString(0));
                        }
                        
                    }
                }
                connection.Close();
            }

        }

        public string userMail;
        public void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "" || TextBox2.Text == "")
            {
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = "SOME REQUIRED FIELDS ARE EMPTY";
            }

            else
            {
                //SqlConnection con = new SqlConnection(@"Data Source=LAPTOP-JJH1ME9N\SQLEXPRESS;Initial Catalog=pear_users;Integrated Security=True");
                SqlConnection con = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_users;Integrated Security=True");
                //con.Open();

                SqlCommand cmd, cmd1;
                //SqlDataAdapter adap = new SqlDataAdapter();
                String sql = "", sql1 = "", role="";
                sql = "select * from userss where email=@mymail and pass=@mypass";
                cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("mymail", TextBox1.Text);
                cmd.Parameters.AddWithValue("mypass", TextBox2.Text);

                sql1 = "select role from userss where email=@mymail";
                cmd1 = new SqlCommand(sql1, con);
                cmd1.Parameters.AddWithValue("mymail", TextBox1.Text);
                

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                con.Open();
                /*int i = */
                cmd.ExecuteNonQuery();

                role = (String)cmd1.ExecuteScalar();

                //string role;
                //using (SqlCommand cmd1 = new SqlCommand("select role from userss where email=@mymail", con))
                //{
                //    cmd1.Parameters.AddWithValue("mymail", TextBox1.Text);
                //    role = (string)cmd1.ExecuteScalar();
                //}
                
                con.Close();

                if (dt.Rows.Count > 0)
                {
                    Session["isLoggedIn"] = "true";
                    Session["Email"] = TextBox1.Text;
                    Session["Trial"] = DropDown1.Text;
                    Session["Role"] = role;
                    Response.Redirect("Account.aspx");

                }
                else
                {
                    Label1.Text = "EMAIL AND PASSWORD INCORRECT";
                    Label1.ForeColor = System.Drawing.Color.Red;
                }
            }

            //cmd.Dispose();
            //con.Close();
        }
    }
}