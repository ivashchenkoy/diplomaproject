﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;

namespace DiplomaProject
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton10_Click(object sender, EventArgs e)
        { 

            if (UploadImages.HasFiles)
            { 
                int i = 0;
                SqlConnection con = new SqlConnection(@"Data Source=aidmed\;Initial Catalog=pear_images;Integrated Security=True");
                con.Open();


                //using (SqlCommand cmd0 = new SqlCommand("select count(trial) from trials where trial=@trial", con))
                //{
                //    cmd0.Parameters.AddWithValue("@trial", TextBox1.Text);
                //    int cnt = (int)cmd0.ExecuteScalar();

                //    TextBox1.Text = cnt.ToString();

                //    if (cnt == 0)
                //    {
                //        SqlCommand cmd1 = new SqlCommand("insert into trials values(@trial)",con);
                //        cmd1.Parameters.AddWithValue("@trial", TextBox1.Text);
                //        cmd1.ExecuteNonQuery();
                //    }
                //}


                foreach (HttpPostedFile uploadedFile in UploadImages.PostedFiles)
                {
                    i++;
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO imagess (image, id, trial, algo) VALUES (@im, @id, @trial, @algo)", con))
                    {

                        Stream fs = uploadedFile.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        byte[] imageData = br.ReadBytes((Int32)fs.Length);

                        //byte[] imageData = File.ReadAllBytes(uploadedFile);
                        cmd.Parameters.AddWithValue("@im", imageData);
                        cmd.Parameters.AddWithValue("@id", i);
                        cmd.Parameters.AddWithValue("@trial", TextBox1.Text);
                        cmd.Parameters.AddWithValue("@algo", TextBox2.Text);
                        cmd.ExecuteNonQuery();

                    }

                }

                con.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('INSERTED SUCCESSFULLY')", true);
                Response.Redirect("Account.aspx");

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('NO SELECTED IMAGES')", true);
            }
        }
    }
}