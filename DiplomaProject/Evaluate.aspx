﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Evaluate.aspx.cs" Inherits="DiplomaProject.Evaluate" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <link runat="server" rel="stylesheet" type="text/css" href="Style_SignUp.css" media="screen" />
    <title>EVALUATE!</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="shortcut icon" type="image/png" href="images/pear_icon.ico" />
    <link runat="server" rel="stylesheet" href="Style_MainPage.css" />
    <link runat="server" rel="stylesheet" type="text/css" href="util.css" />
    
</head>
<body>
    <header id="header">
        <div class="inner">
            <a href="MainPage.aspx" class="logo">PEAR: PicturE Analysis Research</a>
            <nav id="nav">
                <a href="LearnMore.html">learn more</a>
                <a href="Account.aspx">your account</a>
            </nav>
        </div>
    </header>
  <form runat="server">   
    <%--<div class="left-cont m-l-50 m-t-30">--%>

    <div class="left-cont m-l-50 m-t-30">
       
            <div class="m-t-3 m-b-3">
                        <asp:Label ID="Label3" Text="Choose algorithms to compare:" runat="server" />
                    </div>

                    <%--<asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" />--%>

                    <asp:dropdownlist runat="server" id="DropDown1" OnSelectedIndexChanged="DropDown1_SelectedIndexChanged"> 
                         <asp:listitem text="Bicubic interpolation" value="bicubic"></asp:listitem>
                         <asp:listitem text="CLAHE" value="clahe"></asp:listitem>
                         <asp:listitem text="Histogram Equalization" value="equaliz"></asp:listitem>
                         <asp:listitem text="Gaussian Blur" value="gauss"></asp:listitem>
                         <asp:listitem text="Lanczos Interpolation" value="lanczos"></asp:listitem>
                         <asp:listitem text="Linear Interpolation" value="linear"></asp:listitem>
                         <asp:listitem text="Median Filter" value="median"></asp:listitem>
                         <asp:listitem text="Nearest Neighbour Interpolation" value="nn"></asp:listitem>
                         <asp:listitem text="Original" value="preprop"></asp:listitem>
                         <asp:listitem text="Unsharp Masking" value="unsharp"></asp:listitem>
                        <asp:listitem text="Average (Mean) Filter" value="mean"></asp:listitem>
                        <asp:listitem text="U-Net" value="unet"></asp:listitem>
                        <asp:listitem text="Resized" value="res"></asp:listitem>
                        <asp:listitem text="Sharpening Filter" value="sharp"></asp:listitem>
                    </asp:dropdownlist>
                    <asp:dropdownlist runat="server" id="DropDown2" OnSelectedIndexChanged="DropDown2_SelectedIndexChanged"> 
                         <asp:listitem text="Bicubic interpolation" value="bicubic"></asp:listitem>
                         <asp:listitem text="CLAHE" value="clahe"></asp:listitem>
                         <asp:listitem text="Histogram Equalization" value="equaliz"></asp:listitem>
                         <asp:listitem text="Gaussian Blur" value="gauss"></asp:listitem>
                         <asp:listitem text="Lanczos Interpolation" value="lanczos"></asp:listitem>
                         <asp:listitem text="Linear Interpolation" value="linear"></asp:listitem>
                         <asp:listitem text="Median Filter" value="median"></asp:listitem>
                         <asp:listitem text="Nearest Neighbour Interpolation" value="nn"></asp:listitem>
                         <asp:listitem text="Original" value="preprop"></asp:listitem>
                         <asp:listitem text="Unsharp Masking" value="unsharp"></asp:listitem>
                        <asp:listitem text="Average (Mean) Filter" value="mean"></asp:listitem>
                        <asp:listitem text="U-Net" value="unet"></asp:listitem>
                        <asp:listitem text="Resized" value="res"></asp:listitem>
                        <asp:listitem text="Sharpening Filter" value="sharp"></asp:listitem>
                    </asp:dropdownlist>
        <div class="m-t-3 m-b-3">
                        <asp:Label ID="Label4" Text="Compared by:" runat="server" />
                    </div>
        <asp:TextBox runat="server" ID="text1"></asp:TextBox>
        

                <%-- SAVE TO DATABASE PURPOSE --%>
<%--                <asp:LinkButton ID="Button2" runat="server" class="login100-form-btn m-t-10" Text="SAVE TO DB" OnClick="Button2_Click1"/>--%>

                <asp:LinkButton ID="Button3" runat="server" class="login100-form-btn m-t-30" Text="start" OnClick="Button3_Click1"/>

                <asp:LinkButton ID="Button1" runat="server" class="login100-form-btn m-t-10" Text="submit" Visible="false" OnClick="Button1_Click1"/>

                <%--<asp:LinkButton ID="Button5" runat="server" class="login100-form-btn m-t-30" Text="switch trials" OnClick="Button5_Click" />--%>
        </div>
    

    <div class="right-cont m-r-50 m-t-30">
        <asp:Label ID="lbl" Visible="true" Text="" runat="server" />

        <div class="img-left">
            <asp:Image ID="Image1" ImageUrl="./images/sample.png" Width="350px" Height="350px" runat="server" />
        </div>

        <div class="img-right">
            <asp:Image ID="Image2" ImageUrl="./images/sample.png" Width="350px" Height="350px" runat="server" />
        </div>

        <div class="slidecontainer m-l-130 float-l">
            <input type="range" min="0" max="100" value="50" class="slider" id="myRange1">
            <%--<p><span id="demo1"></span> % vs. <span id="demo2"></span> %</p>--%>
            <asp:Label Visible="true" Text="" runat="server" />

            <asp:Label ID="Label1" Text="50" Visible="true" runat="server" />
            <asp:HiddenField ID = "hf1" runat = "server" />

            <asp:Label Visible="true" Text="% vs. " runat="server" />

            <asp:Label ID="Label2" Text="50" Visible="true" runat="server" />
            <asp:HiddenField ID = "hf2" runat = "server" />

            <asp:Label Visible="true" Text="%" runat="server" />
        </div>

    </div>

          
    </form>

    <script>
        var lab1 = document.getElementById("<%=Label1.ClientID %>");
        var lab2 = document.getElementById("<%=Label2.ClientID %>");


        var slider1 = document.getElementById("myRange1");

        lab1.innerHTML = 50;
        lab2.innerHTML = 50;

        document.getElementById("<%=hf1.ClientID %>").value = lab1.innerHTML;
        document.getElementById("<%=hf2.ClientID %>").value = lab2.innerHTML;

        slider1.oninput = function () {

            lab1.innerHTML = this.value;
            lab2.innerHTML = (100 - this.value);

            document.getElementById("<%=hf1.ClientID %>").value = lab1.innerHTML;
            document.getElementById("<%=hf2.ClientID %>").value = lab2.innerHTML;

            //Session["Label1"] = lab1.innerText;
            //Session["Label2"] = lab2.innerText;
        } 

        //function setVar() {

        //    Session["Label1"] = lab1.innerText;
        //    Session["Label2"] = lab2.innerText;
        //}

    </script>

</body>
</html>